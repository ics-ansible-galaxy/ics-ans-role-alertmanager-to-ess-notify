# ics-ans-role-alertmanager-to-ess-notify

Ansible role to install alertmanager-to-ess-notify.


## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-alertmanager-to-ess-notify
```
## Role Variables

```yaml
---
alertmanager_to_ess_notify_repository: https://gitlab.esss.lu.se/ics-infrastructure/alertmanager-to-ess-notify.git
alertmanager_to_ess_notify_main_path: /opt/alertmanager_to_ess_notify
alertmanager_to_ess_notify_version: master
alertmanager_to_ess_notify_env_file: "{{ alertmanager_to_ess_notify_config_dir }}/alertmanager_to_ess_notify_env.yml"
alertmanager_to_ess_notify_config_file: "{{ alertmanager_to_ess_notify_config_dir }}/alertmanager_to_ess_notify.yaml"
alertmanager_to_ess_notify_group: aten
alertmanager_to_ess_notify_user: aten
alertmanager_to_ess_notify_bind: 127.0.0.1:8000
alertmanager_to_ess_notify_config_dir: /etc/alertmanager_to_ess_notify
alertmanager_to_ess_notify_config:
  notify_url: https://notify.esss.lu.se/api/v2/services
  receivers:
    ESSnotify:
      strategy: one_to_one
      service_id: the_service_id
```
## License

BSD 2-clause
