import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service(host):
    s = host.service("alertmanager_to_ess_notify.service")
    assert s.is_running
    assert s.is_enabled


def test_csentry_index(host):
    # This tests that traefik forwards traffic to the csentry web server
    # and that we can access the csentry login page
    cmd = host.command(
        "curl http://localhost:8000/health"
    )
    assert 'true' in cmd.stdout
