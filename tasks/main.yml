---
- name: Ensure git is installed
  package:
    name: git
    state: present

- name: Create alertmanager_to_ess_notify group
  group:
    name: "{{ alertmanager_to_ess_notify_group }}"
    state: present

- name: Create alertmanager_to_ess_notify user
  user:
    name: "{{ alertmanager_to_ess_notify_user }}"
    comment: Alertmanager to ESS notify user
    group: "{{ alertmanager_to_ess_notify_group }}"

- name: Create alertmanager_to_ess_notify config directory
  file:
    path: "{{ alertmanager_to_ess_notify_config_dir }}"
    state: directory
    mode: 0700
    owner: "{{ alertmanager_to_ess_notify_user }}"
    group: "{{ alertmanager_to_ess_notify_group }}"
  notify:
    - Update alertmanager_to_ess_notify conda environment

- name: Create alertmanager_to_ess_notify main directory
  file:
    path: "{{ alertmanager_to_ess_notify_main_path }}"
    state: directory
  notify:
    - Update alertmanager_to_ess_notify conda environment

- name: Checkout the Alertmanager to ESS notify app
  git:
    repo: "{{ alertmanager_to_ess_notify_repository }}"
    dest: "{{ alertmanager_to_ess_notify_main_path }}"
    version: "{{ alertmanager_to_ess_notify_version }}"
    accept_hostkey: true
  notify:
    - Update alertmanager_to_ess_notify conda environment

- name: Copy conda environment file
  template:
    src: alertmanager_to_ess_notify.yaml.j2
    dest: "{{ alertmanager_to_ess_notify_env_file }}"
  notify:
    - Update alertmanager_to_ess_notify conda environment

- name: Flush handlers
  meta: flush_handlers

- name: Deploy configuration
  copy:
    content: "{{ alertmanager_to_ess_notify_config | to_nice_yaml }}"
    dest: "{{ alertmanager_to_ess_notify_config_file }}"
    mode: 0400
    owner: "{{ alertmanager_to_ess_notify_user }}"
    group: "{{ alertmanager_to_ess_notify_group }}"
  notify:
    - Restart Alertmanager to ESS notify

- name: Deploy systemd service
  template:
    src: alertmanager_to_ess_notify.service.j2
    dest: /etc/systemd/system/alertmanager_to_ess_notify.service
    owner: root
    group: root
    mode: 0644
  notify:
    - Restart Alertmanager to ESS notify


- name: Start and enable alertmanager_to_ess_notify
  service:
    name: alertmanager_to_ess_notify
    state: started
    enabled: true
    daemon_reload: true
